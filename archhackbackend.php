<?php
header("Content-Type: application/json");
require 'archhackdblogin.php';

$typeOfReq = (int) @$_GET['reqtype'];
switch ($typeOfReq) {
    case 1:
        $chno = (int) @$_GET['chno'];
        $reply = new KReq1;
        $reply->gdch = 0;
        $chexistchk = $mysqli->prepare("SELECT chno FROM channel WHERE chno=?");
        if(!$chexistchk){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $chexistchk->bind_param('i', $chno);
        $chexistchk->execute();
        $chexistchk->bind_result($ch_db);
        if ($chexistchk->fetch()) {
            $existingchno = $ch_db;
            $reply->gdch = 1;
            $reply->chno = $existingchno;
        }
        $chexistchk->close();
        dataWillTx($reply);
        $reply = null;
        $mysqli->close();
        exit;
        break;
    
    case 2:
        $loc = (string) @$_GET['loc'];
        $wx = (string) @$_GET['wx'];
        $des = (string) @$_GET['des'];
        $reply = new KReq2;
        $putchannel = $mysqli->prepare("INSERT INTO channel (loc, wx, des) VALUES (?, ?, ?)");
        if(!$putchannel){
            $reply->gdnewch = 0;
            $mysqli->close();
            dataWillTx($reply);
            exit;
        }
        $putchannel->bind_param('sss', $loc, $wx, $des);
        $putchannel->execute();
        if (!empty((int)$putchannel->errno)) {
            $reply->gdnewch = 0;
            $putchannel->close();
            $mysqli->close();
            dataWillTx($reply);
            exit;
        }
        else {
            $putchannel->close();
            $searchlast = $mysqli->prepare("SELECT chno FROM channel ORDER BY chno DESC");        //potential problem: multiple async db access
            if(!$searchlast){
                $reply->gdnewch = 0;
                $mysqli->close();
                dataWillTx($reply);
                exit;
            }
            $searchlast->execute();
            $searchlast->bind_result($chno_db);
            if ($searchlast->fetch()) {
                $lastchno = $chno_db;
                $reply->gdnewch = 1;
                $reply->chno = $lastchno;
            }
            else {
                $reply->gdnewch = 0;
                $searchlast->close();
                $mysqli->close();
                dataWillTx($reply);
                exit;
            }
            $searchlast->close();
            $mysqli->close();
            dataWillTx($reply);
            $reply = null;
            exit;
        }
        break;
    
    case 3:
        $chno = (int) @$_GET['chno'];
        $reply = new KReq3;
        $chno = $mysqli->real_escape_string($chno);     //i dont think i need to do this, i already int it
        $reply->chno = $chno;
        $querystr = "SELECT id FROM patientdata WHERE ch='".$chno."'";
        if ($patientcounter = $mysqli->query($querystr)) {
            $pcount = $patientcounter->num_rows;
            $patientcounter->close();
        }
        else {
            $pcount = 0;
            $patientcounter->close();
        }
        if ($pcount > 0) {
            $plist = [];
            $no = [];
            $sta = [];
            $att = [];
            $amb = [];
            $des = [];
            $getlist = $mysqli->prepare("SELECT no, sta, att, amb, des FROM patientdata WHERE ch=? order by id ASC");
            if(!$getlist){
                $reply->parr = [];
                $mysqli->close();
                dataWillTx($reply);
                exit;
            }
            $getlist->bind_param('i', $chno);
            $getlist->execute();
            $getlist->bind_result($no_db, $sta_db, $att_db, $amb_db, $des_db);
            for ($idx = 0; $idx < $pcount; $idx++) {
                $getlist->fetch();
                $no[$idx] = $no_db;
                $sta[$idx] = $sta_db;
                $att[$idx] = $att_db;
                $amb[$idx] = $amb_db;
                $des[$idx] = $des_db;
            }
            $getlist->close();
            for ($idx = 0; $idx < $pcount; $idx++) {
                $whatP = new PData;
                $whatP->pid = $no[$idx];
                $whatP->sta = $sta[$idx];
                $whatP->att = $att[$idx];
                $whatP->amb = $amb[$idx];
                $whatP->des = $des[$idx];
                $plist[$idx] = $whatP;
            }
            $reply->parr = $plist;
            dataWillTx($reply);
            $reply = null;
            $mysqli->close();
            exit;
        }
        else {
            $reply->parr = [];
            dataWillTx($reply);
            $reply = null;
            $mysqli->close();
            exit;
        }
        break;
    case 4:
        $chno = (int) @$_GET['chno'];
        $sta = (int) @$_GET['sta'];
        $des = (string) @$_GET['des'];
        $amb = (int) @$_GET['amb'];
        $att = 0;
        $whatid = 0;
        $reply = new KReq4;
        
        if ($chno > 0) {
            $reply->chno = $chno;
            
            $getmaxid = $mysqli->prepare("SELECT no FROM patientdata WHERE ch=? order by id DESC");
            if(!$getmaxid){
                $reply->opmsg = 0;
                $mysqli->close();
                dataWillTx($reply);
                exit;
            }
            $getmaxid->bind_param('i', $chno);
            $getmaxid->execute();
            $getmaxid->bind_result($no_db);
            if ($getmaxid->fetch()){
                $whatid = $no_db;
            }
            $getmaxid->close();
            
            $whatid += 1;
            
            $putevent = $mysqli->prepare("INSERT INTO patientdata (ch, no, sta, des, att, amb) VALUES (?, ?, ?, ?, ?, ?)");
            if(!$putevent){
                $reply->opmsg = 0;
                $mysqli->close();
                dataWillTx($reply);
                exit;
            }
            $putevent->bind_param('iiisii', $chno, $whatid, $sta, $des, $att, $amb);
            $putevent->execute();
            if (!empty((int)$putevent->errno)) {
                $reply->opmsg = 0;
                $mysqli->close();
                dataWillTx($reply);
                exit;
            }
            else {
                $reply->pid = $whatid;
                $reply->opmsg = 1;
            }
            $putevent->close();
            $mysqli->close();
            dataWillTx($reply);
            exit;
        }
        else {
            $reply->opmsg = 0;
            $reply->chno = null;
            dataWillTx($reply);
            exit;
        }
        break;
    case 5:
        $pid = (int) @$_GET['pid'];
        $chno = (int) @$_GET['chno'];
        
        $reply = new KReq5;
        $reply->chno = $chno;
        $pexistchk = $mysqli->prepare("SELECT no, sta, att, amb, des FROM patientdata WHERE ch=? AND no=?");
        if(!$pexistchk){
            printf("Query Prep Failed: %s\n", $mysqli->error);
            exit;
        }
        $pexistchk->bind_param('ii', $chno, $pid);
        $pexistchk->execute();
        $pexistchk->bind_result($no_db, $sta_db, $att_db, $amb_db, $des_db);
        if ($pexistchk->fetch()) {
            $whatdata = new PData;
            $whatdata->pid = $no_db;
            $whatdata->sta = $sta_db;
            $whatdata->att = $att_db;
            $whatdata->amb = $amb_db;
            $whatdata->des = $des_db;
            $reply->pdata = $whatdata;
        }
        else {
            $reply->pdata = new PData;
        }
        $pexistchk->close();
        dataWillTx($reply);
        $reply = null;
        $mysqli->close();
        exit;
        break;
    case 6:
        $chno = (int) @$_GET['chno'];
        $pid = (int) @$_GET['pid'];
        $sta = (int) @$_GET['sta'];
        $des = (string) @$_GET['des'];
        $att = (int) @$_GET['att'];
        $amb = (int) @$_GET['amb'];
        $reply = new KReq6;
        $reply->chno = $chno;
        $updatepatient = $mysqli->prepare("UPDATE patientdata SET sta=?, att=?, amb=?, des=? WHERE ch=? AND no=?");
        if(!$updatepatient){
            $reply->opmsg = 0;
            $mysqli->close();
            dataWillTx($reply);
            exit;
        }
        $updatepatient->bind_param('iiisii', $sta, $att, $amb, $des, $chno, $pid);
        $updatepatient->execute();
        if (!empty((int)$updatepatient->errno)) {
            $reply->opmsg = 0;
            $updatepatient->close();
            $mysqli->close();
            dataWillTx($reply);
            exit;
        }
        else {
            $reply->pid = $pid;
            $reply->opmsg = 1;
        }
        $updatepatient->close();
        $mysqli->close();
        dataWillTx($reply);
        exit;
        break;
    default:
        exit;
        break;
}

function dataWillTx($data) {
    echo json_encode($data);
}

class PData {
    public $pid = null;
    public $sta = null;
    public $att = null;
    public $amb = null;
    public $des = null;
}

class KReq1 {
    public $gdch = null;
    public $chno = null;
}

class KReq2 {
    public $gdnewch = null;
    public $chno = null;
}

class KReq3 {
    public $parr = [];
    public $chno = null;
}

class KReq4 {
    public $pid = null;
    public $opmsg = null;
    public $chno = null;
}

class KReq5 {
    public $pdata = null;
    public $chno = null;
}

class KReq6 {
    public $pid = null;
    public $opmsg = null;
    public $chno = null;
}

?>
