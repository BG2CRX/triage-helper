## Inspiration
	Over the past decade there have been a myriad of catastrophes that have resulted in the mass casualties of civilians, such the Pulse nightclub shootings, Hurricane Mathew, the bombing in Nice France, etc. When dealing with mass casualty incidents, the Triage system, used to priorities patients in large numbers, is essential in saving the most amount of people with the least amount of resources. With this in mind, as well as the theme of Archhacks 2016 being “Healthtech”, our team yearned to create an app which would make this Triage system more effective and efficient so that more patients can be saved and attended to during their time in need. 

## What it does

In essence, the ios app create a chat room in which members of a healthcare team can access to Triage their patients. As each person is tagged and labeled during Triage, the healthcare team can create entries in the chat room per patient. Each patient entry will have basic information such as; patient status, patient injuries, patient number, if patient is being attended to, and if ambulance is/has been requested to. The app will constantly be updated so that the healthcare team can effectively distribute their care to the patients. 

## How we built it

We used swift, uikit, xcode, php, and amazon-web-service to host sql database.

## Challenges we ran into

Our team consisted of only one ios coder, while the rest of the team had limited to no experience in coding. Training to code with in the 48 hours was a big issue. There was a lot of training on the spot during this project.


## Accomplishments that we're proud of

The app was created by one main coder, with three inexperienced coders, one of which came from a medical background and not from a computer science background. The fact that the app runs on real time is remarkable, taking into consideration the limited skill set that went into creating the app. The workload that was accomplished under 36 hours is an achievement of it self.

## What we learned

Even though a group of people may be inexperienced or unprepared for a project, by collaborating and feeding off each others knowledge, everyone can contribute in some way towards the goal.

## What's next for Triage Helper

Triage Helper will need to be more developed in order to have even more individuals access the app at the same time. 